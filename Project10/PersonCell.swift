//
//  PersonCell.swift
//  Project10
//
//  Created by Ruben Dias on 16/04/2020.
//  Copyright © 2020 Ruben Dias. All rights reserved.
//

import UIKit

class PersonCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var name: UILabel!
    
}
